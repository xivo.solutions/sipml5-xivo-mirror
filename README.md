Project home page: [sipml5.org](http://www.sipml5.org) <br />
Download JS API: [SIPml-api.js](https://raw.githubusercontent.com/DoubangoTelecom/sipml5/master/release/SIPml-api.js)

# XiVO UC/CC Integration

This repository has been modified to include some patches for XiVO UC/CC. The building mechanism has been slightly changed to generate a minified and a non-minified lib.

When releasing, please build the library using the provided `release.sh` script, changing the `API_VERSION` variable accordingly. When reviewing and merging a merge request, do not forget to tag the library with the same version used in `API_VERSION`:

* Release candidate: `git tag -a 2.1.3f-rc -m "2.1.3f Release candidate"`
* Final version: `git tag -a 2.1.3f -m "2.1.3f Final release" 2.1.3f-rc`

This script will generate the minified and non-minified files in `release` folder that need to be copied to the following projects:

* xucserver
* xucmgt
